package fr.upem.partiel.part2.parser
import scala.math.log10
import fr.upem.partiel.part2.model.Movie
import fr.upem.partiel.part2.model.Movie.Country._
import fr.upem.partiel.part2.model.Movie._
import play.api.libs.functional.syntax._
import play.api.libs.json._

import scala.util.{Success, Try}

object Parser {


  // TODO
  def toDirector: String => Option[Director] = name => {
    name.split(" ") match {
      case Array(a, b) => Option(Director(a, b))
      case _ => Option.empty
    }
  }

  // TODO
  def toName: String => Title = s => Title(s)

  // TODO
  def toCountry: String => Option[Country] = s => s match{
    case "FR" => Some(France)
    case "UK" =>  Some(England)
    case "IT" => Some(Italy)
    case "GE" => Some(Germany)
    case "US" =>  Some(UnitedStates)
    case _ => None
  }

  // TODO
  def toYear: String => Option[Year] = annee =>{
    if((annee.startsWith("1") || annee.startsWith("2"))
      && annee.length == 4){
      val y = Try(annee.toInt)
      y match {
      case Success(v) => Some(Year(v))
      case _ => None
      }
    }
    None
  }




  // TODO
  def toViews: BigDecimal => Option[Views] = b => b.longValue() match{
    case b if(b >= 0) => Some(Views(b))
    case _ => None
  }

  implicit val directorReads = Reads[Director] {
    case JsString(value) => toDirector(value).map(JsSuccess(_)).getOrElse(JsError("Not a valid Director"))
    case _ => JsError("Not a valid type for Director")
  }

  implicit val nameReads = Reads[Title] {
    case JsString(value) => JsSuccess(toName(value))
    case _ => JsError("Not a valid type for Name")
  }

  implicit val countryReads = Reads[Country] {
    case JsString(value) => toCountry(value).map(JsSuccess(_)).getOrElse(JsError("Not a valid Country"))
    case _ => JsError("Not a valid type for Country")
  }

  implicit val yearReads = Reads[Year] {
    case JsString(value) => toYear(value).map(JsSuccess(_)).getOrElse(JsError("Not a valid Year"))
    case _ => JsError("Not a valid type for Year")
  }

  implicit val viewsReads = Reads[Views] {
    case JsNumber(value) => toViews(value).map(JsSuccess(_)).getOrElse(JsError("Not a valid Views"))
    case _ => JsError("Not a valid type for Views")
  }

  implicit val movieReads: Reads[Movie] = (
    (__ \ "title").read[Title] and
      (__ \ "director").read[Director] and
      (__ \ "year").read[Year] and
      (__ \ "views").read[Views] and
      (__ \ "country").read[Country]
    ) (Movie.apply _)

}
