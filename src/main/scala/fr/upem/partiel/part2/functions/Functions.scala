package fr.upem.partiel.part2.functions

import fr.upem.partiel.part2.model.Movie
import fr.upem.partiel.part2.model.Movie.Director

object Functions {

  // TODO
  lazy val getDirectorNames: List[Movie] => List[String] = {
    case h :: t => h.director.name::getDirectorNames(t)
    case Nil => Nil
  }

  // TODO
  lazy val viewMoreThan: Long => List[Movie] => List[Movie] = l => list => list match {
    case h::tail => if(h.views.value > l){
      h::viewMoreThan(l)(tail)
    }else{
      viewMoreThan(l)(tail)
    }
    case Nil => Nil
  }

  // TODO
  lazy val byDirector: List[Movie] => Map[Director, List[Movie]] = movies => {
    movies.groupBy(_.director)
  }

}
